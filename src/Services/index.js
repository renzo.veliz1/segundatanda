const Controllers = require("../Controllers");



async function Create({name, age, color}){
	try{
		let {statusCode, data, message} = await Controllers.Create({name, age, color});
		return { statusCode, data, message};
	}catch(error){
		console.log({step:'servicio Create', error:error.toString()})
		return {statusCode:500, message:error.toString()};
	}
}

async function Update({name, age, color, id}){
	try{
		let {statusCode, data, message} = await Controllers.Update({name, age, color, id});
		return { statusCode, data, message};
	}catch(error){
		console.log({step:'servicio Update', error:error.toString()})
		return {statusCode:500, message:error.toString()};
	}
}

async function Delete({id}){
	try{
		let {statusCode, data, message} = await Controllers.Delete({where:{id}});
		return { statusCode, data, message};
	}catch(error){
		console.log({step:'servicio Delete', error:error.toString()})
		return {statusCode:500, message:error.toString()};
	}
}

async function FindOne({name}){
	try{
		let {statusCode, data, message} = await Controllers.FindOne({where:{name}});
		return { statusCode, data, message};
	}catch(error){
		console.log({step:'servicio FindOne', error:error.toString()})
		return {statusCode:500, message:error.toString()};
	}
}

async function View({}){
	try{
		let {statusCode, data, message} = await Controllers.View({});
		return { statusCode, data, message};
	}catch(error){
		console.log({step:'servicio View', error:error.toString()})
		return {statusCode:500, message:error.toString()};
	}
}

module.exports= {Create,
	Update,
	Delete,
	FindOne,
	View
};