const {InternalError} = require('../settings');
const Servicio = require('../Services');
const {
	queueCreate,
	queueUpdate,
	queueDelete,
	queueFindOne,
	queueView
} = require('./index');


queueCreate.process(async function(job, done){
	try{
		const { age, color, name } = job.data;
		console.log(job.data);
		let {statusCode, data, message} = await Servicio.Create({ age, color, name });
		//devolvemos en la respuesta el objeto
		done(null, {statusCode, data, message});
	}catch(error){
		console.log({step: "adapter queuCreate process", error: error.toString()});
		//si hubo algun error devolvemos el error en la respuesta
		done(null, {statusCode: 500, message:InternalError});
	}
});

queueUpdate.process(async function(job, done){
	try{
		const { age, color, name, id } = job.data;
		console.log(job.data);
		let {statusCode, data, message} = await Servicio.Update({ age, color, name, id });
		//devolvemos en la respuesta el objeto
		done(null, {statusCode, data, message});
	}catch(error){
		console.log({step: "adapter queueUpdate process", error: error.toString()});
		//si hubo algun error devolvemos el error en la respuesta
		done(null, {statusCode: 500, message:InternalError});
	}
});

queueDelete.process(async function(job, done){
	try{
		const { id } = job.data;
		console.log(job.data);
		let {statusCode, data, message} = await Servicio.Delete({id});
		//devolvemos en la respuesta el objeto
		done(null, {statusCode, data, message});
	}catch(error){
		console.log({step: "adapter queuDelete process", error: error.toString()});
		//si hubo algun error devolvemos el error en la respuesta
		done(null, {statusCode: 500, message:InternalError});
	}
});

queueFindOne.process(async function(job, done){
	try{
		const { name } = job.data;
		console.log(job.data);
		let {statusCode, data, message} = await Servicio.FindOne({ name });
		//devolvemos en la respuesta el objeto
		done(null, {statusCode, data, message});
	}catch(error){
		console.log({step: "adapter queuFindOne process", error: error.toString()});
		//si hubo algun error devolvemos el error en la respuesta
		done(null, {statusCode: 500, message:InternalError});
	}
});

queueView.process(async function(job, done){
	try{
		const { } = job.data;
		console.log(job.data);
		let {statusCode, data, message} = await Servicio.View({});
		//devolvemos en la respuesta el objeto
		done(null, {statusCode, data, message});
	}catch(error){
		console.log({step: "adapter queuView process", error: error.toString()});
		//si hubo algun error devolvemos el error en la respuesta
		done(null, {statusCode: 500, message:InternalError});
	}
});