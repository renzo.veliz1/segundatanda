const {Adatador} = require('./src/Adapters');
const Main =  async() => {
	try{
		//buscamos el usuario por ID
		const result=await Adatador({id:1});

		//devolvemos el mensaje de error que nos mando adapter
		if (result.statusCode!==200) throw (result.message);

		//caso de no haber error imprimimos los resultados
		console.log("Usuario :", result.data);

	}catch(error){
		//en caso de error rompemos el vidrio
		console.log(error);
	}
}

Main();